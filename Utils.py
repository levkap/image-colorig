import numpy as np
from skimage.color import lab2rgb
import matplotlib.pyplot as plt
from tensorflow import keras

# Saves original vs fake image (gray scale format)
def save_images(gray, fakes, original, epoch, batch, dir_path='results'):
    fakes = (fakes + 1) / 2.0
    original = (original + 1) / 2.0
    gray = (gray + 1) / 2.0
    for i in range(len(fakes)):
        path = dir_path + "/epoch_" + str(epoch) + "_batch_" + str(batch) + "_" + str(i) + ".png"
        fig, ax = plt.subplots(1, 3)
        ax[0].axis('off')
        ax[0].set_title('Gray')
        ax[0].imshow(gray[i, ..., 0])
        ax[1].axis('off')
        ax[1].set_title('Fake')
        ax[1].imshow(fakes[i])
        ax[2].axis('off')
        ax[2].set_title('Original')
        ax[2].imshow(original[i])
        fig.savefig(path)
        plt.close(fig)


# Saves original vs fake image (lab format)
def save_images_lab(fake_LAB, LAB, epoch, batch, dir_path='results'):
    LAB[:, :, :, 0] = (LAB[:, :, :, 0] + 1) * 50
    l = LAB[:, :, :, 0]
    l = l[..., np.newaxis]
    fake = np.concatenate([l, fake_LAB], axis=-1)
    gray = np.concatenate([l, np.zeros(fake_LAB.shape)], axis=-1)
    fake[:, :, :, 1:] = fake[:, :, :, 1:] * 128
    LAB[:, :, :, 1:] = LAB[:, :, :, 1:] * 128

    for i in range(len(fake)):
        path = dir_path + "/epoch_" + str(epoch) + "_batch_" + str(batch) + "_" + str(i) + ".png"
        fig, ax = plt.subplots(1, 3)
        ax[0].axis('off')
        ax[0].set_title('Gray')
        ax[0].imshow(lab2rgb(gray[i]))
        ax[1].axis('off')
        ax[1].set_title('Fake')
        ax[1].imshow(lab2rgb(fake[i]))
        ax[2].axis('off')
        ax[2].set_title('Original')
        ax[2].imshow(lab2rgb(LAB[i]))
        fig.savefig(path)
        plt.close(fig)


def new_gen(original_data_gen, gray_data_gen):
    while True:
        yield (next(original_data_gen), next(gray_data_gen))


# Creates generator of images, if True returns original and gray scale image
# otherwise returns only the original image.
def create_gen(path, img_size=32, batch_size=32, rotation_range=20, gray_image=True):
    rnd_seed = 42

    image_generator = keras.preprocessing.image.ImageDataGenerator(rotation_range=rotation_range, horizontal_flip=True,
                                                                   vertical_flip=True, rescale=1. / 255)
    original_data_gen = image_generator.flow_from_directory(directory=path, batch_size=batch_size, seed=rnd_seed,
                                                            shuffle=True, target_size=(img_size, img_size))
    gray_data_gen = image_generator.flow_from_directory(directory=path, batch_size=batch_size, color_mode='grayscale',
                                                        seed=rnd_seed, shuffle=True, target_size=(img_size, img_size))

    gen_gen = new_gen(original_data_gen, gray_data_gen)
    num_of_batches = len(gray_data_gen)

    if gray_image:
        return gen_gen, num_of_batches
    else:
        return original_data_gen, num_of_batches
