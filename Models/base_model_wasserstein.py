from tensorflow import keras


def wasserstein_loss(y_pred, Y_true):
    return keras.backend.mean(y_pred * Y_true)


def make_generator(gray_img_shape):
    gray_img = keras.layers.Input(shape=gray_img_shape)
    y = keras.layers.Conv2D(32, kernel_size=3, strides=2, padding="same")(gray_img)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    e1 = keras.layers.BatchNormalization()(y, training=True)

    y = keras.layers.Conv2D(64, kernel_size=3, strides=2, padding="same")(e1)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    e2 = keras.layers.BatchNormalization()(y, training=True)

    y = keras.layers.Conv2D(128, kernel_size=3, strides=2, padding="same")(e2)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    y = keras.layers.BatchNormalization()(y, training=True)

    # y = keras.layers.UpSampling2D()(y)
    # y = keras.layers.Conv2D(64, kernel_size=3, strides=1, padding="same")(y)
    y = keras.layers.Conv2DTranspose(64, kernel_size=3, strides=2, padding="same")(y)
    y = keras.layers.BatchNormalization()(y, training=True)
    y = keras.layers.Dropout(0.5)(y)
    y = keras.layers.Concatenate()([y, e2])
    y = keras.layers.LeakyReLU(alpha=0.2)(y)

    # y = keras.layers.UpSampling2D()(y)
    # y = keras.layers.Conv2D(32, kernel_size=3, strides=1, padding="same")(y)
    y = keras.layers.Conv2DTranspose(32, kernel_size=3, strides=2, padding="same")(y)
    y = keras.layers.BatchNormalization()(y, training=True)
    y = keras.layers.Dropout(0.5)(y)
    y = keras.layers.Concatenate()([y, e1])
    y = keras.layers.LeakyReLU(alpha=0.2)(y)

    # y = keras.layers.UpSampling2D()(y)
    # y = keras.layers.Conv2D(2, kernel_size=3, strides=1, padding="same")(y)
    y = keras.layers.Conv2DTranspose(2, kernel_size=3, strides=2, padding="same")(y)
    colored_img = keras.layers.Activation("tanh")(y)
    model = keras.models.Model(gray_img, colored_img)
    model.summary()
    return model


def make_discriminator(color_img_shape):
    color_img = keras.layers.Input(shape=color_img_shape)
    y = keras.layers.Conv2D(32, kernel_size=4, padding="same")(color_img)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    y = keras.layers.Dropout(0.25)(y)
    y = keras.layers.Conv2D(64, kernel_size=4, strides=2, padding="same")(y)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    y = keras.layers.Dropout(0.25)(y)

    y = keras.layers.Conv2D(128, kernel_size=4, strides=2, padding="same")(y)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    y = keras.layers.Dropout(0.25)(y)

    y = keras.layers.Flatten()(y)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    # y = keras.layers.Dense(1, activation='sigmoid')(y)
    y = keras.layers.Dense(1, activation='linear')(y)
    model = keras.models.Model(color_img, y)
    model.summary()

    return model


def make_combine(rgb_shape, gray_shape, case=1):
    """
    case 1 : single wasserstein loss
    case 2 : double loss, wasserstein loss and mae without weights
    case 3 : double loss, wasserstein loss and mae with weights
    """
    # Define Discriminator
    discriminator = make_discriminator(rgb_shape)

    # Settings
    optimizer = keras.optimizers.Adam(lr=0.0002, beta_1=0.5, beta_2=0.9)
    discriminator.compile(loss=wasserstein_loss, optimizer=optimizer, metrics=['accuracy'])
    discriminator.trainable = False

    # Define Generator and Combine
    generator = make_generator(gray_shape)
    gen_Input = keras.layers.Input(shape=gray_shape)
    gen_output = generator(gen_Input)
    dis_output_for_gen = discriminator(gen_output)

    if case == 1:
        ganModel = keras.models.Model(inputs=gen_Input, outputs=dis_output_for_gen)
        ganModel.compile(loss=wasserstein_loss, optimizer=optimizer, metrics=['accuracy'])
    elif case == 2:
        ganModel = keras.models.Model(inputs=gen_Input, outputs=[dis_output_for_gen, gen_output])
        ganModel.compile(loss=[wasserstein_loss, 'mae'], optimizer=optimizer,
                         metrics=['accuracy'])
    else:
        ganModel = keras.models.Model(inputs=gen_Input, outputs=[dis_output_for_gen, gen_output])
        ganModel.compile(loss=[wasserstein_loss, 'mae'], loss_weights=[100, 0.1], optimizer=optimizer,
                         metrics=['accuracy'])
    discriminator.trainable = True
    return ganModel, generator, discriminator
