import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow.python.keras.layers.merge import _Merge
from tensorflow import keras


class GradientPenalty(tf.keras.layers.Layer):
    def call(self, inputs):
        target, wrt = inputs
        grad = K.gradients(target, wrt)[0]
        return K.sqrt(K.sum(K.batch_flatten(K.square(grad)),
                            axis=1, keepdims=True)) - 1

    def compute_output_shape(self, input_shapes):
        return (input_shapes[1][0], 1)


class RandomWeightedAverage(_Merge):
    def build(self, input_shape):
        super(RandomWeightedAverage, self).build(input_shape)
        if len(input_shape) != 2:
            raise ValueError('A `RandomWeightedAverage` layer should be '
                             'called on exactly 2 inputs')

    def _merge_function(self, inputs):
        if len(inputs) != 2:
            raise ValueError('A `RandomWeightedAverage` layer should be '
                             'called on exactly 2 inputs')

        (x, y) = inputs
        shape = K.shape(x)
        weights = K.random_uniform(shape[:1], 0, 1)
        for i in range(len(K.int_shape(x)) - 1):
            weights = K.expand_dims(weights, -1)
        return x * weights + y * (1 - weights)


class Attention(tf.keras.layers.Layer):
    def __init__(self, ch, **kwargs):
        super(Attention, self).__init__(**kwargs)
        self.channels = ch
        self.filters_f_g = self.channels // 8
        self.filters_h = self.channels

    def build(self, input_shape):
        kernel_shape_f_g = (1, 1) + (self.channels, self.filters_f_g)
        print(kernel_shape_f_g)
        kernel_shape_h = (1, 1) + (self.channels, self.filters_h)

        # Create a trainable weight variable for this layer:
        self.gamma = self.add_weight(name='gamma', shape=[1], initializer='zeros', trainable=True)
        self.kernel_f = self.add_weight(shape=kernel_shape_f_g,
                                        initializer='glorot_uniform',
                                        name='kernel_f')
        self.kernel_g = self.add_weight(shape=kernel_shape_f_g,
                                        initializer='glorot_uniform',
                                        name='kernel_g')
        self.kernel_h = self.add_weight(shape=kernel_shape_h,
                                        initializer='glorot_uniform',
                                        name='kernel_h')
        self.bias_f = self.add_weight(shape=(self.filters_f_g,),
                                      initializer='zeros',
                                      name='bias_F')
        self.bias_g = self.add_weight(shape=(self.filters_f_g,),
                                      initializer='zeros',
                                      name='bias_g')
        self.bias_h = self.add_weight(shape=(self.filters_h,),
                                      initializer='zeros',
                                      name='bias_h')
        super(Attention, self).build(input_shape)
        # Set input spec.
        self.input_spec = tf.keras.layers.InputSpec(ndim=4,
                                                    axes={3: input_shape[-1]})
        self.built = True

    def call(self, x):
        def hw_flatten(x):
            return K.reshape(x, shape=[K.shape(x)[0], K.shape(x)[1] * K.shape(x)[2], K.shape(x)[-1]])

        f = K.conv2d(x,
                     kernel=self.kernel_f,
                     strides=(1, 1), padding='same')  # [bs, h, w, c']
        f = K.bias_add(f, self.bias_f)
        g = K.conv2d(x,
                     kernel=self.kernel_g,
                     strides=(1, 1), padding='same')  # [bs, h, w, c']
        g = K.bias_add(g, self.bias_g)
        h = K.conv2d(x,
                     kernel=self.kernel_h,
                     strides=(1, 1), padding='same')  # [bs, h, w, c]
        h = K.bias_add(h, self.bias_h)

        s = tf.matmul(hw_flatten(g), hw_flatten(f), transpose_b=True)  # # [bs, N, N]

        beta = K.softmax(s, axis=-1)  # attention map

        o = K.batch_dot(beta, hw_flatten(h))  # [bs, N, C]

        o = K.reshape(o, shape=K.shape(x))  # [bs, h, w, C]
        x = self.gamma * o + x

        return x

    def compute_output_shape(self, input_shape):
        return input_shape


class selfAttention(tf.keras.layers.Layer):
    def __init__(self, n_head, hidden_dim, penalty=0.1, **kwargs):
        self.n_head = n_head
        self.P = penalty

        self.hidden_dim = hidden_dim
        super(selfAttention, self).__init__(**kwargs)

    def build(self, input_shape):
        self.W1 = self.add_weight(name='w1', shape=(input_shape[2], self.hidden_dim), initializer='uniform',
                                  trainable=True)
        self.W2 = self.add_weight(name='W2', shape=(self.hidden_dim, self.n_head), initializer='uniform',
                                  trainable=True)
        super(selfAttention, self).build(input_shape)

    def call(self, x, **kwargs):
        d1 = K.dot(x, self.W1)
        tanh1 = K.tanh(d1)
        d2 = K.dot(tanh1, self.W2)
        softmax1 = K.softmax(d2, axis=0)
        A = K.permute_dimensions(softmax1, (0, 2, 1))
        emb_mat = K.batch_dot(A, x, axes=[2, 1])
        reshape = K.batch_flatten(emb_mat)
        eye = K.eye(self.n_head)
        prod = K.batch_dot(softmax1, A, axes=[1, 2])
        self.add_loss(self.P * K.sqrt(K.sum(K.square(prod - eye))))
        return reshape

    def compute_output_shape(self, input_shape):
        return (input_shape[0], input_shape[-1] * self.n_head,)


def wasserstein_loss(y_pred, Y_true):
    return keras.backend.mean(y_pred * Y_true)


def make_generator(gray_img_shape):
    gray_img = keras.layers.Input(shape=gray_img_shape)
    y = keras.layers.Conv2D(32, kernel_size=3, strides=2, padding="same")(gray_img)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    e1 = keras.layers.BatchNormalization()(y, training=True)
    # 64
    y = keras.layers.Conv2D(64, kernel_size=3, strides=2, padding="same")(e1)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    e2 = keras.layers.BatchNormalization()(y, training=True)
    # 32
    y = keras.layers.Conv2D(128, kernel_size=3, strides=2, padding="same")(e2)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    e3 = keras.layers.BatchNormalization()(y, training=True)
    # 16
    y = keras.layers.Conv2D(256, kernel_size=3, strides=2, padding="same")(e3)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    e4 = keras.layers.BatchNormalization()(y, training=True)
    # # 8
    y = keras.layers.Conv2D(512, kernel_size=3, strides=2, padding="same")(e4)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    y = keras.layers.BatchNormalization()(y, training=True)
    # # 4
    y = keras.layers.Conv2DTranspose(256, kernel_size=3, strides=2, padding="same")(y)
    y = keras.layers.BatchNormalization()(y, training=True)
    # y = keras.layers.Dropout(0.5)(y, training=True)
    y = keras.layers.Concatenate()([y, e4])
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    # 16
    y = keras.layers.Conv2DTranspose(128, kernel_size=3, strides=2, padding="same")(y)
    y = keras.layers.BatchNormalization()(y, training=True)
    y = keras.layers.Dropout(0.5)(y, training=True)
    y = keras.layers.Concatenate()([y, e3])
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    # 32
    # y = keras.layers.UpSampling2D()(y)
    # y = keras.layers.Conv2D(64, kernel_size=3, strides=1, padding="same")(y)
    y = keras.layers.Conv2DTranspose(64, kernel_size=3, strides=2, padding="same")(y)
    y = keras.layers.BatchNormalization()(y, training=True)
    y = keras.layers.Dropout(0.5)(y)
    y = keras.layers.Concatenate()([y, e2])
    y = keras.layers.LeakyReLU(alpha=0.2)(y)

    # y = keras.layers.UpSampling2D()(y)
    # y = keras.layers.Conv2D(32, kernel_size=3, strides=1, padding="same")(y)
    y = keras.layers.Conv2DTranspose(32, kernel_size=3, strides=2, padding="same")(y)
    y = keras.layers.BatchNormalization()(y, training=True)
    y = keras.layers.Dropout(0.5)(y)
    y = keras.layers.Concatenate()([y, e1])
    y = keras.layers.LeakyReLU(alpha=0.2)(y)

    # y = keras.layers.UpSampling2D()(y)
    # y = keras.layers.Conv2D(2, kernel_size=3, strides=1, padding="same")(y)
    y = keras.layers.Conv2DTranspose(2, kernel_size=3, strides=2, padding="same")(y)
    colored_img = keras.layers.Activation("tanh")(y)
    model = keras.models.Model(gray_img, colored_img)
    model.summary()
    return model


def make_discriminator(color_img_shape):
    color_img = keras.layers.Input(shape=color_img_shape)
    y = keras.layers.Conv2D(32, kernel_size=3, padding="same")(color_img)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    y = keras.layers.Dropout(0.25)(y)
    y = keras.layers.Conv2D(64, kernel_size=3, strides=2, padding="same")(y)
    # y = selfAttention(64)(y)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    y = keras.layers.Dropout(0.25)(y)
    y = keras.layers.Conv2D(128, kernel_size=4, strides=2, padding="same")(y)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    y = keras.layers.Dropout(0.25)(y)

    y = keras.layers.Flatten()(y)
    y = keras.layers.LeakyReLU(alpha=0.2)(y)
    y = keras.layers.Dense(1)(y)
    model = keras.models.Model(color_img, y)
    model.summary()

    return model


def make_combine(rgb_shape, gray_shape, case=1):
    # disable eager
    tf.compat.v1.disable_eager_execution()
    # Settings
    optimizer = keras.optimizers.Adam(lr=0.0001, beta_1=0.5, beta_2=0.9)

    # Define Generator
    discriminator = make_discriminator(rgb_shape)
    generator = make_generator(gray_shape)

    for layer in discriminator.layers:
        layer.trainble = False
    discriminator.trainable = False

    generator_input = keras.layers.Input(shape=gray_shape)
    generator_output = generator(generator_input)
    discriminator_output_for_generator = discriminator(generator_output)
    if case == 1:
        generator_model = keras.models.Model(inputs=[generator_input],
                                             outputs=[discriminator_output_for_generator])
        generator_model.compile(optimizer=optimizer, loss=wasserstein_loss, metrics=['accuracy'])
    elif case == 2:
        generator_model = keras.models.Model(inputs=[generator_input],
                                             outputs=[discriminator_output_for_generator, generator_output])
        generator_model.compile(optimizer=optimizer, loss=[wasserstein_loss, 'mae'], metrics=['accuracy'])
    else:
        generator_model = keras.models.Model(inputs=[generator_input],
                                             outputs=[discriminator_output_for_generator, generator_output])
        generator_model.compile(optimizer=optimizer, loss=[wasserstein_loss, 'mae'], loss_weights=[100, 0.1],
                                metrics=['accuracy'])

    for layer in discriminator.layers:
        layer.trainable = True
    for layer in generator.layers:
        layer.trainable = False

    discriminator.trainable = True
    generator.trainable = False

    real_input = keras.layers.Input(shape=rgb_shape)
    generator_input_for_discriminator = keras.layers.Input(shape=gray_shape)
    fake_generated_input_for_discriminator = generator(generator_input_for_discriminator)
    discriminator_output_for_fake_generated = discriminator(fake_generated_input_for_discriminator)
    discriminator_output_for_real_input = discriminator(real_input)

    avg_images = RandomWeightedAverage()([real_input, fake_generated_input_for_discriminator])
    disc_avg = discriminator(avg_images)
    gradient_penalty = GradientPenalty()([disc_avg, avg_images])
    discriminator_model = keras.models.Model(inputs=[real_input, generator_input_for_discriminator],
                                             outputs=[discriminator_output_for_real_input,
                                                      discriminator_output_for_fake_generated, gradient_penalty])
    discriminator_model.compile(optimizer=optimizer, loss=[wasserstein_loss, wasserstein_loss, 'mse'])

    return generator_model, generator, discriminator_model
