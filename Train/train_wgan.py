import numpy as np
import time
from Models.base_model_wasserstein import make_combine
from Utils import create_gen, save_images_lab
from skimage.color import rgb2lab
import os

# Settings
epochs = 50
img_size = 64
batch_size = 16
plot_interval = 200
rotation_range = 45
num_to_plot = 1
rnd_seed = 42
add_noise = False
discriminator_train_ratio = 5
case = 2
"""
case 1 : single wasserstein loss
case 2 : double loss, wasserstein loss and mae without weights
case 3 : double loss, wasserstein loss and mae with weights
"""
save_dir_path = os.path.join(os.pardir, 'results')
read_dir_path = os.path.join(os.pardir, 'pokemon')
face_read_dir_path = os.path.join(os.pardir, 'faces')

# Create result's directory
if not os.path.exists(os.path.join(os.pardir, save_dir_path)):
    os.makedirs(os.path.join(os.pardir, save_dir_path))

# Initialize Generator and Discriminator, combine model
combine, generator, discriminator = make_combine((img_size, img_size, 2), (img_size, img_size, 1), case=case)

# Initialize Dataset
combine_gen, num_of_batches = create_gen(read_dir_path, batch_size=batch_size, img_size=img_size, gray_image=False)

# Define parameters
d_runs = 0
g_runs = 0

avg_d_loss = 0.0
avg_g_loss = 0.0

mean = 0
sigma = 0.1

print(num_of_batches)

for epoch in range(epochs):

    tic = time.clock()
    counter = 0
    for i in range(501):

        original_batch = next(combine_gen)

        LAB_batch = rgb2lab(original_batch[0])

        # Rescale to range -1 : 1
        LAB_batch[:, :, :, 0] = (LAB_batch[:, :, :, 0] / 50) - 1
        LAB_batch[:, :, :, 1:] = (LAB_batch[:, :, :, 1:] / 128)
        L_batch = LAB_batch[:, :, :, 0]
        L_batch = L_batch[..., np.newaxis]

        valid = np.ones(2 * len(LAB_batch)) * 0.9
        valid[len(LAB_batch):] *= -1

        # Create fake images
        fake_LAB = generator(L_batch)

        # Create noise for original images
        if add_noise:
            noise = np.random.normal(mean, sigma, LAB_batch[:, :, :, 1:].shape)
            noised_AB = np.clip(LAB_batch[:, :, :, 1:] + noise, -1, 1)
            combined_LAB = np.concatenate([noised_AB, fake_LAB])
        else:
            combined_LAB = np.concatenate([LAB_batch[:, :, :, 1:], fake_LAB])

        # Train the discriminator
        d_loss = discriminator.train_on_batch(combined_LAB, valid)
        d_runs += 1

        # train the generator
        if case == 1:
            g_loss = combine.train_on_batch(L_batch, -np.ones(len(L_batch)))
        else:
            g_loss = combine.train_on_batch(L_batch, [-np.ones(len(L_batch)), LAB_batch[:, :, :, 1:]])
        g_runs += 1

        # save images
        if counter % plot_interval == 0:
            # Create fake images
            save_images_lab(np.clip(fake_LAB[:num_to_plot], -1, 1), LAB_batch[:num_to_plot], epoch, counter,
                            save_dir_path)
        counter += 1
    # Plot progress
    print("%d [D loss: %f, acc.: %.2f%%] [G loss: %f]" % (epoch, d_loss[0], 100 * d_loss[1], g_loss[0]))
    print('epoch {} finished in {} seconds'.format(epoch, time.clock() - tic))
