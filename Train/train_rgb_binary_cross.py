import numpy as np
import time
from Models.base_model_binary_cross import make_combine
from Utils import create_gen, save_images
import os

# Settings
epochs = 50
img_size = 64
batch_size = 16
plot_interval = 200
rotation_range = 45
num_to_plot = 1
rnd_seed = 42
case = 2
add_noise = False
"""
case 1 : single binary cross entropy
case 2 : double binary cross entropy without weights
case 3 : double binary cross entropy with weights
case 4 : double loss, binary cross entropy and mae with weights
"""
save_dir_path = os.path.join(os.pardir, 'results')
read_dir_path = os.path.join(os.pardir, 'pokemon')
face_read_dir_path = os.path.join(os.pardir, 'faces')

# Create result's directory
if not os.path.exists(os.path.join(os.pardir, save_dir_path)):
    os.makedirs(os.path.join(os.pardir, save_dir_path))

# Initialize Generator and Discriminator, combine model
combine, generator, discriminator = make_combine((img_size, img_size, 3), (img_size, img_size, 1), out_channel=3,
                                                 case=case)

# Initialize Dataset
combine_gen, num_of_batches = create_gen(face_read_dir_path, batch_size=batch_size, img_size=img_size, gray_image=True)

# Define parameters
d_runs = 0
g_runs = 0

mean = 0
sigma = 0.1
print(num_of_batches)

for epoch in range(epochs):

    tic = time.clock()
    counter = 0

    for i in range(num_of_batches * 5):
        # Generates Fakes
        o_batch, g_batch = next(combine_gen)
        original_batch = o_batch[0]
        gray_batch = g_batch[0]

        # Rescale to range -1 : 1
        original_batch = (original_batch * 2) - 1
        gray_batch = (gray_batch * 2) - 1


        # Create fake images
        fake_batch = np.clip(generator(gray_batch), -1, 1)

        # Create noise for original images
        if add_noise:
            noise = np.random.normal(mean, sigma, original_batch.shape)
            noised_batch = np.clip(original_batch + noise, -1, 1)
            combined = np.concatenate([noised_batch, fake_batch])
            combined = np.concatenate([original_batch, fake_batch])
        else:
            combined = np.concatenate([original_batch, fake_batch])

        # Train combined
        valid = np.ones(2 * len(original_batch)) * 0.1
        valid[:len(original_batch)] = 0.9

        # train the discriminator
        d_loss = discriminator.train_on_batch(combined, valid)
        if (case == 1):
            g_loss = combine.train_on_batch(gray_batch, np.ones(len(gray_batch)))
        else:
            g_loss = combine.train_on_batch(gray_batch, [np.ones(len(gray_batch)), original_batch])

        # save images
        if counter % plot_interval == 0:
            save_images(gray_batch[:num_to_plot], fake_batch[:num_to_plot], original_batch[:num_to_plot], epoch,
                        counter, save_dir_path)
        counter += 1
    # Plot progress
    print("%d [D loss: %f, acc.: %.2f%%] [G loss: %f]" % (epoch, d_loss[0], 100 * d_loss[1], g_loss[0]))
    print('epoch {} finished in {} seconds'.format(epoch, time.clock() - tic))
