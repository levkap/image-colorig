# Image Coloring

Image coloring of Pokemon images using GAN. 

pokemon dataset https://www.kaggle.com/kvpratama/pokemon-images-dataset

faces dataset https://www.kaggle.com/ciplab/real-and-fake-face-detection

Download and save the datasets in main directory with names 'pokemon' and 'faces'.

I have tried to color pokemon images using GAN models. The best results were achieved by  
base model with Binary Crossentropy.
WGAN with GP could achieve better results than the base model but becuase it's more complicated model it 
requires more examples to train on. 

***Some Results:***


!['result'](/Results/Binary_Cross_&_MAE_batch_size_16/epoch_10.png )
!['result'](/Results/Binary_Cross_&_MAE_batch_size_16/epoch_40.png )
!['result'](/Results/WGAN_GP_batch_size_32/epoch_10_batch_0.png )
!['result'](/Results/Single_Binary_Cross_batch_size_16/epoch_20.png )
!['result'](/Results/Single_Binary_Cross_batch_size_16/epoch_40.png )


